import { Injectable } from '@angular/core';
import { Platform, LoadingController } from '@ionic/angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { IonElement } from '../interface/ion-element';
import { tween, styler, spring, easing, stagger } from 'popmotion';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  public animate: Array<any>;
  public loading: any;

  constructor(
    private platform: Platform,
    private nativePageTransitions: NativePageTransitions,
    public loadingController: LoadingController
  ) {}

  async ngOnInit() {
    this.loading = await this.loadingController.create({});
  }

  slidePage(ob?: NativeTransitionOptions) {
    if(!this.platform.is('cordova')) { return; }
    return new Promise(async next => {
      let options: NativeTransitionOptions = Object.assign({
        direction: 'up',
        duration: 200,
        slowdownfactor: 10,
        slidePixels: 300,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      }, ob);

      let sR = await this.nativePageTransitions.slide(options);
      next(sR);
    });
  }

  wait(n: number) {
    return new Promise(async next => {
      setTimeout(() => {
        next(true);
      }, 100 * n);
    });
  }

  public doAnimate = async (list: IonElement, next?: any) => {
    const stylers = Array.from(list.el.children).filter((el: any) => el.classList.contains('op-0')).map((el: any) => Object.assign(styler(el), { el }));
    if(!stylers.length) { return; }

    const hide = Array(stylers.length).fill(tween({
      from: { x: 0 },
      to: { x: 300, scale: 0 }
    }));
    const animate = Array(stylers.length).fill(tween({
      to: { x: 0, scale: 1 },
      from: { x: 300 },
      duration: 200,
    }));

    stagger(hide, 0).start((v: any) => v.forEach((x: any, i: number) => stylers[i].set(x)));
    await this.wait(5);
    stagger(animate, 100).start((v: any) => v.forEach((x: any, i: number) => {
      stylers[i].el.classList.remove('op-0');
      stylers[i].set(x);
    }));
    await this.wait(stylers.length);
    next && next();
  }

  public showSearch = async (navigator: IonElement, search: IonElement, next?: any) => {
    const navstyler = Object.assign(styler(navigator.el), { el: navigator.el });
    const searchstyler = Object.assign(styler(search.el), { el: search.el });

    tween({
      from: { x: 0, scale: 1 },
      to: { x: -navigator.el.clientWidth, scale: 1 },
      ease: easing.anticipate,
      duration: 1000,
    }).start({
      update: (v: any) => navstyler.set(v),
      complete: () => {}
    });

    tween({
      from: { x: search.el.clientWidth, scale: 1 },
      to: { x: 0, scale: 1 },
      ease: easing.anticipate,
      duration: 1000
    }).start({
      update: (v: any) => searchstyler.set(v),
      complete: () => {}
    });

    await this.wait(10);
    next && next();
  }

  public hideSearch = async (navigator: IonElement, search: IonElement, next?: any) => {
    const navstyler = Object.assign(styler(navigator.el), { el: navigator.el });
    const searchstyler = Object.assign(styler(search.el), { el: search.el });

    tween({
      from: { x: -navigator.el.clientWidth, scale: 1 },
      to: { x: 0, scale: 1 },
      ease: easing.anticipate,
      duration: 1000,
    }).start({
      update: (v: any) => navstyler.set(v)
    });

    tween({
      from: { x: 0, scale: 1 },
      to: { x: search.el.clientWidth, scale: 1 },
      ease: easing.anticipate,
      duration: 1000
    }).start({
      update: (v: any) => searchstyler.set(v)
    });

    await this.wait(10);
    next && next();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      mode: 'ios',
      spinner: 'lines',
      translucent: false,
      showBackdrop: false,
      cssClass: 'custom-loading',
    });
    this.loading.present();

    // const { role, data } = await loading.onDidDismiss();
    // console.log('Loading dismissed!');
  }

  async dismissLoading() {
    this.loading.dismiss();
  }

}
