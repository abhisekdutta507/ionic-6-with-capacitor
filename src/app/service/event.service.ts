import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { Network } from '@ionic-native/network/ngx';

let that: any;

@Injectable({
  providedIn: 'root'
})
export class EventService {
  
  private connectSubscription: any;
  private disconnectSubscription: any;
  private backButtonAsyncEvent: any;
  private exit: boolean = true;

  constructor(
    private platform: Platform,
    private appMinimize: AppMinimize,
    private network: Network
  ) {
    that = this;
  }

  bindBackButton() {
    // https://stackoverflow.com/questions/51700879/handling-hardware-back-button-in-ionic3-vs-ionic4
    this.backButtonAsyncEvent = this.platform.backButton.subscribe(async (e: any) => {
      if (that.exit) { this.appMinimize.minimize(); /* navigator['app'].exitApp(); */ }
      // else { this.action.presentNativeToast({message: 'Press again to exit', duration: 1000}); }

      // that.exit = true;
      
      // setTimeout(() => { that.exit = false; }, 1000);
    });

    // console.log('back button subscribed', that.backButtonAsyncEvent);
  }

  releaseBackButton() {
    this.backButtonAsyncEvent.unsubscribe();

    // console.log('back button unsubscribed', that.backButtonAsyncEvent);
  }

  bindNetworkStatus(connect?: any, disconnect?: any) {
    // watch network for a connection
    this.connectSubscription = this.network.onConnect().subscribe(connect.bind(this.network));
    this.disconnectSubscription = this.network.onDisconnect().subscribe(disconnect);
  }

  releaseNetworkStatus() {
    this.connectSubscription.unsubscribe();
    this.disconnectSubscription.unsubscribe();
  }
}
