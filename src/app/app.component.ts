import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/' },
    { title: 'Socket.io', url: '/socket-io' },
    { title: 'Settings', url: '/settings' },
    { title: 'About', url: '/about' }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();

      // app version control
      // https://github.com/ionic-team/capacitor/issues/840
      
      this.statusBar.overlaysWebView(false);

      this.statusBar.backgroundColorByHexString('#ffffff');
      this.statusBar.styleDefault();
    });
  }
}
