import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';

// import { VideoPlayer } from '@ionic-native/video-player/ngx';
// import { FilePath } from '@ionic-native/file-path/ngx';
// import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';

import { Device } from '@ionic-native/device/ngx';

import { Hotspot } from '@ionic-native/hotspot/ngx';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Sim } from '@ionic-native/sim/ngx';

import { Clipboard } from '@ionic-native/clipboard/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    File,
    // FilePath,
    Device,

    Sim,
    Network,
    Hotspot,
    NetworkInterface,

    StatusBar,
    Clipboard,

    // FileOpener,
    // VideoPlayer,
    AppMinimize,
    SplashScreen,
    NativePageTransitions,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
