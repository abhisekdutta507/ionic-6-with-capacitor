import { Component, ViewChild } from '@angular/core';
import { UiService } from '../../service/ui.service';
import { EventService } from '../../service/event.service';
import { IonElement } from '../../interface/ion-element';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  @ViewChild('list', { static: false }) list?: IonElement;
  @ViewChild('navigator', { static: false }) navigator?: IonElement;
  @ViewChild('searchBar', { static: false }) searchBar?: IonElement;

  public items: Array<any>;

  private inIt = [
    'Pokémon Yellow',
    'Mega Man X',
    'The Legend of Zelda',
    'Pac-Man',
    'Super Mario World'
  ];

  public search: string = '';

  constructor(
    private Ui: UiService,
    private event: EventService,
  ) {
    this.items = [];
  }

  async ionViewDidEnter() {
    let { Ui, event, inIt } = this;
    event.bindBackButton();

    if(!this.items.length) {
      // present the loading controller
      Ui.presentLoading();
      // initialize the storage
      this.items = inIt;
      await Ui.wait(5);
      Ui.dismissLoading();
      Ui.doAnimate(this.list);
    }
  }

  async ionViewWillLeave() {
    this.event.releaseBackButton();
  }

  async ngOnDestroy() {
    this.event.releaseBackButton();
  }

  public doRefresh = async (event) => {
    let { Ui, inIt } = this;
    // present the loading controller
    // Ui.presentLoading();
    // empty the storage
    this.items = [];
    await Ui.wait(5);
    // initialize the storage
    this.items = inIt;
    await Ui.wait(5);
    // Ui.dismissLoading();
    Ui.doAnimate(this.list);
    // complete the refresh event
    event.target.complete();
  }

  async onSearch() {
    let { Ui, inIt, search } = this;
    // filter the items
    this.items = inIt.filter(el => el.toLowerCase().includes(search.toLowerCase()));
    await Ui.wait(1);
    Ui.dismissLoading();
    Ui.doAnimate(this.list);
  }

  async showSearch() {
    let { Ui, navigator, searchBar } = this;
    Ui.showSearch(navigator, searchBar);
  }

  async hideSearch() {
    let { Ui, navigator, searchBar } = this;
    Ui.hideSearch(navigator, searchBar);
  }

}
