import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AboutSimPage } from './about-sim.page';

describe('AboutSimPage', () => {
  let component: AboutSimPage;
  let fixture: ComponentFixture<AboutSimPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutSimPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AboutSimPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
