import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AboutSimPageRoutingModule } from './about-sim-routing.module';

import { AboutSimPage } from './about-sim.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutSimPageRoutingModule
  ],
  declarations: [AboutSimPage]
})
export class AboutSimPageModule {}
