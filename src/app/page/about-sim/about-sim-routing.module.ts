import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutSimPage } from './about-sim.page';

const routes: Routes = [
  {
    path: '',
    component: AboutSimPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AboutSimPageRoutingModule {}
