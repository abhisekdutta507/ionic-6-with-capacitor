import { Component, OnInit, ViewChild } from '@angular/core';
import { Sim } from '@ionic-native/sim/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';

@Component({
  selector: 'app-about-sim',
  templateUrl: './about-sim.page.html',
  styleUrls: ['./about-sim.page.scss'],
})
export class AboutSimPage implements OnInit {
  @ViewChild('list', { static: false }) list?: IonElement;

  public items: Array<any>;

  constructor(
    private Ui: UiService,
    private sim: Sim,
    private clipboard: Clipboard
  ) { }

  async ngOnInit() {
    await this.Ui.slidePage();
  }

  async ionViewDidEnter() {
    let { Ui, sim } = this;

    if(!this.items) {
      // present the loading controller
      Ui.presentLoading();
      // load sim card data
      let info = await sim.getSimInfo();
      // initialize the storage
      this.items = info.cards.map(({ displayName, phoneNumber }) => ({ displayName, phoneNumber }));
      await Ui.wait(5);
      Ui.dismissLoading();
      Ui.doAnimate(this.list);
    }
  }

  public doRefresh = async (event) => {
    let { Ui, sim } = this;
    // present the loading controller
    // Ui.presentLoading();
    // empty the storage
    this.items = [];
    await Ui.wait(5);
    // load sim card data
    let info = await sim.getSimInfo();
    // initialize the storage
    this.items = info.cards.map(({ displayName, phoneNumber }) => ({ displayName, phoneNumber }));
    await Ui.wait(5);
    // Ui.dismissLoading();
    Ui.doAnimate(this.list);
    // complete the refresh event
    event.target.complete();
  }

  copy(text: string) {
    this.clipboard.copy(text);
  }

}
