import { Component, OnInit, ViewChild } from '@angular/core';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';

@Component({
  selector: 'app-socket-io',
  templateUrl: './socket-io.page.html',
  styleUrls: ['./socket-io.page.scss'],
})
export class SocketIoPage implements OnInit {
  @ViewChild('list', { static: false }) list?: IonElement;
  @ViewChild('navigator', { static: false }) navigator?: IonElement;
  @ViewChild('searchBar', { static: false }) searchBar?: IonElement;

  public items: Array<any>;
  public search: string = '';

  constructor(
    private Ui: UiService,
  ) {
    this.items = [];
  }

  async ngOnInit() {
    await this.Ui.slidePage();
    // request for the sim card permission
  }

  async ionViewDidEnter() {
    let { Ui } = this;

    if(!this.items.length) {
      // present the loading controller
      Ui.presentLoading();
      // initialize the storage
      this.items = [];
      await Ui.wait(5);
      Ui.dismissLoading();
    }
  }

  public doRefresh = async (event) => {
    let { Ui } = this;
    // empty the storage
    this.items = [];
    await Ui.wait(5);
    // complete the refresh event
    event.target.complete();
  }

  async onSearch() {
    
  }

  async showSearch() {
    let { Ui, navigator, searchBar } = this;
    Ui.showSearch(navigator, searchBar);
  }

  async hideSearch() {
    let { Ui, navigator, searchBar } = this;
    Ui.hideSearch(navigator, searchBar);
  }

}
