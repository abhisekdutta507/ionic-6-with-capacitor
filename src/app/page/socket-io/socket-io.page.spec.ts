import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SocketIoPage } from './socket-io.page';

describe('SocketIoPage', () => {
  let component: SocketIoPage;
  let fixture: ComponentFixture<SocketIoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocketIoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SocketIoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
