import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SocketIoPageRoutingModule } from './socket-io-routing.module';
import { SocketIoPage } from './socket-io.page';

import { ChatModule } from '../../module/chat/chat.module';

@NgModule({
  declarations: [SocketIoPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SocketIoPageRoutingModule,
    // ChatModule
  ],
  exports: []
})
export class SocketIoPageModule {}
