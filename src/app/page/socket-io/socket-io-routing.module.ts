import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SocketIoPage } from './socket-io.page';

const routes: Routes = [
  {
    path: '',
    component: SocketIoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SocketIoPageRoutingModule {}
