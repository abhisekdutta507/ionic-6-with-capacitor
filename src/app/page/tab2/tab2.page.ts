import { Component, ViewChild } from '@angular/core';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  @ViewChild('list', { static: false }) list?: IonElement;

  public cards: Array<number>;

  private addEnable: boolean = true;

  constructor(
    private Ui: UiService
  ) {
  }

  private ionViewDidEnter = async () => {
    let { Ui } = this;
    if(!this.cards) {
      // present the loading controller
      Ui.presentLoading();
      // initialize the storage
      this.addEnable = false;
      this.cards = [0, 1, 3, 4];
      await Ui.wait(5);
      Ui.dismissLoading();
      Ui.doAnimate(this.list, () => {
        // console.log('add enabled');
        this.addEnable = true;
      });
    }
  }

  public doRefresh = async (event) => {
    let { Ui } = this;
    // present the loading controller
    // Ui.presentLoading();
    // empty the storage
    this.addEnable = false;
    this.cards = [];
    await Ui.wait(5);
    // initialize the storage
    this.cards = [0, 1, 3, 4];
    await Ui.wait(5);
    // Ui.dismissLoading();
    Ui.doAnimate(this.list, () => {
      // console.log('add enabled');
      this.addEnable = true;
    });
    // complete the refresh event
    event.target.complete();
  }

  public addCard = async (event: Event) => {
    let { cards, Ui } = this;
    // do nothing if clicked
    if(!this.addEnable) { return; }
    // update the storage
    this.addEnable = false;
    cards.push(cards.length);
    await Ui.wait(5);
    Ui.doAnimate(this.list, () => {
      // console.log('add enabled');
      this.addEnable = true;
    });
  }

}
