import { Component, OnInit, ViewChild } from '@angular/core';
import { Sim } from '@ionic-native/sim/ngx';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  @ViewChild('list', { static: false }) list?: IonElement;
  @ViewChild('navigator', { static: false }) navigator?: IonElement;
  @ViewChild('searchBar', { static: false }) searchBar?: IonElement;


  public items: Array<any>;
  private inIt = [
    {
      label: 'Phone',
      url: '/settings/about-phone'
    },
    {
      label: 'Sim',
      url: '/settings/about-sim'
    },
    {
      label: 'Network',
      url: '/settings/about-network'
    }
  ];
  public search: string = '';

  constructor(
    private Ui: UiService,
    private sim: Sim
  ) {
    this.items = [];
  }

  async ngOnInit() {
    await this.Ui.slidePage();
    // request for the sim card permission
    this.sim.requestReadPermission();
  }

  async ionViewWillLeave() {}

  async ionViewDidEnter() {
    let { Ui, inIt } = this;

    if(!this.items.length) {
      // present the loading controller
      Ui.presentLoading();
      // initialize the storage
      this.items = inIt;
      await Ui.wait(5);
      Ui.dismissLoading();
      Ui.doAnimate(this.list);
    }
  }

  public doRefresh = async (event) => {
    let { Ui, inIt } = this;
    // present the loading controller
    // Ui.presentLoading();
    // empty the storage
    this.items = [];
    await Ui.wait(5);
    // initialize the storage
    this.items = inIt;
    await Ui.wait(5);
    // Ui.dismissLoading();
    Ui.doAnimate(this.list);
    // complete the refresh event
    event.target.complete();
  }

  async onSearch() {
    let { Ui, inIt, search } = this;
    // filter the items
    this.items = inIt.filter(el => el.label.toLowerCase().includes(search.toLowerCase()));
    await Ui.wait(1);
    Ui.dismissLoading();
    Ui.doAnimate(this.list);
  }

  async showSearch() {
    let { Ui, navigator, searchBar } = this;
    Ui.showSearch(navigator, searchBar);
  }

  async hideSearch() {
    let { Ui, navigator, searchBar } = this;
    Ui.hideSearch(navigator, searchBar);
  }

}
