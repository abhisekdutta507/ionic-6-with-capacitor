import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsPage } from './settings.page';

const routes: Routes = [
  {
    path: '',
    component: SettingsPage
  },
  {
    path: 'about-phone',
    loadChildren: () => import('../about-phone/about-phone.module').then( m => m.AboutPhonePageModule)
  },
  {
    path: 'about-network',
    loadChildren: () => import('../about-network/about-network.module').then( m => m.AboutNetworkPageModule)
  },
  {
    path: 'about-sim',
    loadChildren: () => import('../about-sim/about-sim.module').then( m => m.AboutSimPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsPageRoutingModule {}
