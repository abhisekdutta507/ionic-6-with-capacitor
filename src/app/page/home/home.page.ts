import { Component, OnInit } from '@angular/core';
import { UiService } from '../../service/ui.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private Ui: UiService
  ) { }

  async ngOnInit() {
    await this.Ui.slidePage();
  }

}
