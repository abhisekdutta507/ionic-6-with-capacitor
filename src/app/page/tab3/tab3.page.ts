import { Component, ViewChild } from '@angular/core';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  @ViewChild('list', { static: false }) list?: IonElement;

  public cards: Array<number>;

  constructor(
    private Ui: UiService
  ) {}

  private ionViewDidEnter = async () => {
    let { Ui } = this;
    if(!this.cards) {
      // present the loading controller
      Ui.presentLoading();
      // initialize the storage
      this.cards = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
      await Ui.wait(5);
      Ui.dismissLoading();
      Ui.doAnimate(this.list);
    }
  }

  public doRefresh = async (event) => {
    let { Ui } = this;
    // present the loading controller
    // Ui.presentLoading();
    // empty the storage
    this.cards = [];
    await Ui.wait(5);
    // initialize the storage
    this.cards = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    await Ui.wait(5);
    // Ui.dismissLoading();
    Ui.doAnimate(this.list);
    // complete the refresh event
    event.target.complete();
  }

}
