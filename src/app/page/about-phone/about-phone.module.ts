import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AboutPhonePageRoutingModule } from './about-phone-routing.module';

import { AboutPhonePage } from './about-phone.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutPhonePageRoutingModule
  ],
  declarations: [AboutPhonePage]
})
export class AboutPhonePageModule {}
