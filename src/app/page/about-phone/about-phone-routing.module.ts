import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutPhonePage } from './about-phone.page';

const routes: Routes = [
  {
    path: '',
    component: AboutPhonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AboutPhonePageRoutingModule {}
