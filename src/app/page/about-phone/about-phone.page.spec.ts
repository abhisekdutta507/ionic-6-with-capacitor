import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AboutPhonePage } from './about-phone.page';

describe('AboutPhonePage', () => {
  let component: AboutPhonePage;
  let fixture: ComponentFixture<AboutPhonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutPhonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AboutPhonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
