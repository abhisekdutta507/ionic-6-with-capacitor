import { Component, OnInit, ViewChild } from '@angular/core';
import { Device } from '@ionic-native/device/ngx';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';

@Component({
  selector: 'app-about-phone',
  templateUrl: './about-phone.page.html',
  styleUrls: ['./about-phone.page.scss'],
})
export class AboutPhonePage implements OnInit {
  @ViewChild('list', { static: false }) list?: IonElement;

  public items: Array<any>;
  private inIt: Array<any>;

  constructor(
    private Ui: UiService,
    private device: Device
  ) {
    this.inIt = [
      {
        label: 'Model',
        note: device.model
      },
      {
        label: 'Platform',
        note: device.platform
      },
      {
        label: 'Version',
        note: device.version
      },
      {
        label: 'UUID',
        note: device.uuid
      },
      {
        label: 'Manufacturer',
        note: device.manufacturer
      },
      {
        label: 'Cordova',
        note: device.cordova
      }
    ];
  }

  async ngOnInit() {
    await this.Ui.slidePage();
  }

  async ionViewDidEnter() {
    let { Ui, inIt } = this;

    if(!this.items) {
      // present the loading controller
      Ui.presentLoading();
      // initialize the storage
      this.items = inIt;
      await Ui.wait(5);
      Ui.dismissLoading();
      Ui.doAnimate(this.list);
    }
  }

  public doRefresh = async (event) => {
    let { Ui, inIt } = this;
    // present the loading controller
    // Ui.presentLoading();
    // empty the storage
    this.items = [];
    await Ui.wait(5);
    // initialize the storage
    this.items = inIt;
    await Ui.wait(5);
    // Ui.dismissLoading();
    Ui.doAnimate(this.list);
    // complete the refresh event
    event.target.complete();
  }

}
