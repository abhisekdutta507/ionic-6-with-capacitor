import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AboutNetworkPageRoutingModule } from './about-network-routing.module';

import { AboutNetworkPage } from './about-network.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutNetworkPageRoutingModule
  ],
  declarations: [AboutNetworkPage]
})
export class AboutNetworkPageModule {}
