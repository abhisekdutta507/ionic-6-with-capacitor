import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AboutNetworkPage } from './about-network.page';

describe('AboutNetworkPage', () => {
  let component: AboutNetworkPage;
  let fixture: ComponentFixture<AboutNetworkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutNetworkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AboutNetworkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
