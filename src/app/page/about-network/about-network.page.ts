import { Component, OnInit, ViewChild } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { NetworkInterface } from '@ionic-native/network-interface/ngx';
import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot/ngx';
import { EventService } from '../../service/event.service';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';

let that: any;

@Component({
  selector: 'app-about-network',
  templateUrl: './about-network.page.html',
  styleUrls: ['./about-network.page.scss'],
})
export class AboutNetworkPage implements OnInit {
  @ViewChild('list', { static: false }) list?: IonElement;

  public items: Array<any>;
  private inIt: Array<any>;

  constructor(
    private Ui: UiService,
    private event: EventService,
    private network: Network,
    private networkInterface: NetworkInterface,
    private hotspot: Hotspot
  ) {
    that = this;
    this.inIt = [
      {
        label: 'Type',
        note: network.type === 'none' ? 'disconnected' : network.type
      },
    ];
  }

  async ngOnInit() {
    await this.Ui.slidePage();
  }

  async ionViewDidEnter() {
    let { Ui, event } = this;
    event.bindNetworkStatus(this.connected, this.disconnected);

    if(!this.items) {
      // present the loading controller
      Ui.presentLoading();
      let wifi = await this.networkInterface.getWiFiIPAddress().catch(e => {});
      if(wifi) {
        const na = [
          {
            label: 'IP',
            note: wifi.ip
          },
          {
            label: 'Subnet',
            note: wifi.subnet
          }
        ];
        this.inIt = [ ...this.inIt, ...na ];
      }
      let con = await this.hotspot.getConnectionInfo().catch(e => {});
      if(con) {
        const na = [
          {
            label: 'SSID',
            note: con.SSID
          },
          {
            label: 'BSSID',
            note: con.BSSID
          }
        ];
        this.inIt = [ ...this.inIt, ...na ];
      }
      // initialize the storage
      this.items = this.inIt;
      await Ui.wait(5);
      Ui.dismissLoading();
      Ui.doAnimate(this.list);
    }
  }

  async ionViewWillLeave() {
    this.event.releaseNetworkStatus();
  }

  async ngOnDestroy() {
    this.event.releaseNetworkStatus();
  }

  public doRefresh = async (event) => {
    let { Ui, inIt, network } = this;
    // present the loading controller
    // Ui.presentLoading();
    // empty the storage
    this.items = [];
    await Ui.wait(5);
    // initialize the object
    that.inIt = [
      {
        label: 'Type',
        note: network.type
      },
    ];
    let wifi = await this.networkInterface.getWiFiIPAddress().catch(e => {});
    if(wifi) {
      const na = [
        {
          label: 'IP',
          note: wifi.ip
        },
        {
          label: 'Subnet',
          note: wifi.subnet
        }
      ];
      this.inIt = [ ...this.inIt, ...na ];
    }
    let con = await this.hotspot.getConnectionInfo().catch(e => {});
    if(con) {
      const na = [
        {
          label: 'SSID',
          note: con.SSID
        },
        {
          label: 'BSSID',
          note: con.BSSID
        }
      ];
      this.inIt = [ ...this.inIt, ...na ];
    }
    // initialize the storage
    this.items = this.inIt;
    await Ui.wait(5);
    // Ui.dismissLoading();
    Ui.doAnimate(this.list);
    // complete the refresh event
    event.target.complete();
  }

  async connected() {
    let { Ui } = this;
    // present the loading controller
    Ui.presentLoading();
    this.items = [];
    await Ui.wait(5);
    // here this is pointing to the network object
    const network: any = this;
    that.inIt = [
      {
        label: 'Type',
        note: network.type
      },
    ];
    let wifi = await that.networkInterface.getWiFiIPAddress().catch(e => {});
    if(wifi) {
      const na = [
        {
          label: 'IP',
          note: wifi.ip
        },
        {
          label: 'Subnet',
          note: wifi.subnet
        }
      ];
      that.inIt = [ ...that.inIt, ...na ];
    }
    let con = await this.hotspot.getConnectionInfo().catch(e => {});
    if(con) {
      const na = [
        {
          label: 'SSID',
          note: con.SSID
        },
        {
          label: 'BSSID',
          note: con.BSSID
        }
      ];
      this.inIt = [ ...this.inIt, ...na ];
    }
    that.items = that.inIt;
    await Ui.wait(5);
    Ui.dismissLoading();
    Ui.doAnimate(this.list);
  }

  async disconnected() {
    const network: any = this;
    that.inIt = [
      {
        label: 'Type',
        note: 'disconnected'
      },
    ];
    that.items = that.inIt;
  }

}
