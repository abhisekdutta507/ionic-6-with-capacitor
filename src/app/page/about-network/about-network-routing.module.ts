import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutNetworkPage } from './about-network.page';

const routes: Routes = [
  {
    path: '',
    component: AboutNetworkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AboutNetworkPageRoutingModule {}
