import { Component, ViewChild, OnInit } from '@angular/core';
import { UiService } from '../../service/ui.service';
import { IonElement } from '../../interface/ion-element';
// import { VideoPlayer } from '@ionic-native/video-player/ngx';
// import { FilePath } from '@ionic-native/file-path/ngx';
// import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  @ViewChild('list', { static: false }) list?: IonElement;

  public cards: Array<any>;
  private inIt: Array<any> = [
    {
      subtitle: 'App Development',
      title: 'Ionic Framework',
      description: 'Ionic Framework is an open source UI toolkit for building performant, high-quality mobile and desktop apps using web technologies.',
      link: 'https://www.youtube.com/watch?v=YwSzqeBchEc&t=537s'
    }
  ];

  constructor(
    private Ui: UiService,
    // private player: VideoPlayer,
    // private filePath: FilePath,
    // private fileOpener: FileOpener,
    private file: File
  ) {
    this.cards = [];
  }

  async ngOnInit() {
    await this.Ui.slidePage();
  }

  async ionViewDidEnter() {
    let { Ui } = this;
    if(!this.cards.length) {
      // present the loading controller
      // Ui.presentLoading();
      await Ui.wait(15);
      // initialize the storage
      this.cards = this.inIt;
      // await Ui.wait(5);
      // Ui.dismissLoading();
      // Ui.doAnimate(this.list);
    }
  }

  async ionViewWillLeave() {}

  async tap() {
    // this.filePath.resolveNativePath('file:///assets/video/ionicrameworkcourse.mp4')
    // .then(filePath => console.log('redmi file path', filePath))
    // .catch(err => console.log({ err }));

    // Playing a video.
    // this.player.play('file:///assets/video/ionicrameworkcourse.mp4').then(() => {
    //   console.log('video completed');
    // }).catch(err => {
    //   console.log('video play error', err);
    // });

    // this.fileOpener.open('file:///assets/video/ionicrameworkcourse.mp4', 'video/mp4')
    // .then(() => console.log('File is opened'))
    // .catch(e => console.log('Error opening file', e));
  }

  async doRefresh(event: any) {
    // await this.Ui.wait(10);
    event.target.complete();
  }

}
