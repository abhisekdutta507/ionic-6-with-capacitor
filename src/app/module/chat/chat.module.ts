import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/**
 * @description Socket Modules for Social Chat.
 */
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
/**
 * @description domain update with production type.
 */
const apiurl = 'http://localhost:5000';
/**
 * @description Select domain available only on Heroku.
 */
const config: SocketIoConfig = { url: apiurl, options: {} };

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SocketIoModule.forRoot(config)
  ],
  exports: [
    /**
     * @description Shared modules are stored here.
     */
    SocketIoModule,
  ]
})
export class ChatModule { }
